//
//  Asset.swift
//  radio_core
//
//  Created by Cody Nelson on 4/3/18.
//  Copyright © 2018 Bonneville. All rights reserved.
//

/*
 Abstract:
 A simple class that holds information about an Asset.
 */
import AVFoundation

public class Asset {
    
    /// The AVURLAsset corresponding to this Asset.
    public var urlAsset: AVURLAsset
    
    /// The underlying `Stream` associated with the Asset based on the contents of the `Streams.plist` entry.
    public let stream: Stream
    
    init(stream: Stream, urlAsset: AVURLAsset) {
        self.urlAsset = urlAsset
        self.stream = stream
    }
}

/// Extends `Asset` to conform to the `Equatable` protocol.
extension Asset: Equatable {
    public static func ==(lhs: Asset, rhs: Asset) -> Bool {
        return (lhs.stream == rhs.stream) && (lhs.urlAsset == rhs.urlAsset)
    }
}


/**
 Extends `Asset` to add a simple download state enumeration used by the sample
 to track the download states of Assets.
 */
extension Asset {
    public enum DownloadState: String {
        
        /// The asset is not downloaded at all.
        case notDownloaded
        
        /// The asset has a download in progress.
        case downloading
        
        /// The asset is downloaded and saved on diek.
        case downloaded
    }
}

/**
 Extends `Asset` to define a number of values to use as keys in dictionary lookups.
 */
extension Asset {
    public struct Keys {
        /**
         Key for the Asset name, used for `AssetDownloadProgressNotification` and
         `AssetDownloadStateChangedNotification` Notifications as well as
         AssetListManager.
         */
        public static let name = "AssetNameKey"
        
        /**
         Key for the Asset download percentage, used for
         `AssetDownloadProgressNotification` Notification.
         */
        public static let percentDownloaded = "AssetPercentDownloadedKey"
        
        /**
         Key for the Asset download state, used for
         `AssetDownloadStateChangedNotification` Notification.
         */
        public static let downloadState = "AssetDownloadStateKey"
        
        /**
         Key for the Asset download AVMediaSelection display Name, used for
         `AssetDownloadStateChangedNotification` Notification.
         */
        public static let downloadSelectionDisplayName = "AssetDownloadSelectionDisplayNameKey"
    }
}

