//
//  Stream.swift
//  radio_core
//
//  Created by Cody Nelson on 4/3/18.
//  Copyright © 2018 Bonneville. All rights reserved.
//

/*
 Abstract:
 A simple class that represents an entry from the `Streams.plist` file in the main application bundle.
 */

import Foundation

public class Stream: Codable {
    
    // MARK: Types
    
    public enum CodingKeys : String, CodingKey {
        case name = "name"
        case playlistURL = "playlist_url"
    }
    
    // MARK: Properties
    
    /// The name of the stream.
    public let name: String
    
    /// The URL pointing to the HLS stream.
    public let playlistURL: String
}

extension Stream: Equatable {
    public static func ==(lhs: Stream, rhs: Stream) -> Bool {
        return (lhs.name == rhs.name) && (lhs.playlistURL == rhs.playlistURL)
    }
}
